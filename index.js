const express = require("express");

const app = express();

app.use(express.json()) //express.json is a method that allows us to handle stream of data from our client and receive the data and automatically parse the incoming data from request. Used to run middlewares (functions that add features to our applications)


const port = 4000;

//collection for courses

let courses = [

    {
        name: "Python 101",
        description: "Learn Python",
        price: 25000
    },
    {
        name: "ReactJS 101",
        description: "Learn React",
        price: 35000
    },
    {
        name: "ExpressJS 101",
        description: "Learn ExpressJS",
        price: 28000
    }

];



let users = [

	{
		email: "marybell_knight",
		password: "merrymarybell"
	},
	{
		email: "janedoePriest",
		password: "jobPriest100"
	},
	{
		email: "kimTofu",
		password: "dubuTofu"
	}

]



//ROUTE

app.get('/',(req,res)=>{

	res.send("Hello from our first ExpressJS route!");

})

app.post('/',(req,res)=>{

	res.send("Hello from our first Express Post Method Route!")
})

//MINI ACTIVITY

app.put('/',(req,res)=>{

	res.send("Hello from a put method route!")
})

app.delete('/',(req,res)=>{

	res.send("Hello from delete route!")

})

app.get('/courses',(req,res)=>{

	res.send(courses);
})



//create a route to add new course

app.post('/courses',(req,res)=>{

	// console.log(req.body) //object, contains the body of request or the input passed

	courses.push(req.body);

	console.log(courses);

	res.send(courses);
})



//MINI ACTIVITY

app.get('/users',(req,res)=>{

	res.send(users);

})

app.post('/users',(req,res)=>{

	console.log(req.body)

	users.push(req.body);

	console.log(users);

	users.pop(req.body);

	res.send(users);

})




app.listen(port,() => console.log('Express API running at port 4000'))




















